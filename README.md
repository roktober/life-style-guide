# Life style guide

## Гайды о жизни 

### [Гайд жизни](./life_guide.md)

### [Реальность и ты](./reality_and_you.md)

## Информация

### [Источкики информации](./useful_information.md)